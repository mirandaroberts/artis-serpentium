<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Requests\AnimalRequest;
use App\Services\AnimalService;
use File;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Intervention\Image\ImageManagerStatic as Image;

class AnimalsController extends Controller
{
    /**
     * @var AnimalService
     */
    protected $service;

    /**
     * AnimalsController constructor.
     * @param AnimalService $service
     */
    public function __construct(AnimalService $service)
    {
        $this->service = $service;
    }

    /**
     * Show the homepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $animals = $this->service->getAll();
        return view('admin.animals.index', compact('animals'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        if ($id == 'create') $animal = $this->service->model;
        else $animal = $this->service->get($id);
        return view('admin.animals.update', compact('animal'));
    }

    /**
     * @param $id
     * @param AnimalRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate($id, AnimalRequest $request)
    {
        if ($id == 'create') $entity = $this->service->create($request->all());
        else $entity = $this->service->update($this->service->get($id), $request->all());
        if ($entity) session()->flash('success', 'Success');
        else session()->flash('error', 'There was an error with your request');
        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getImages($id)
    {
        if (!$animal = $this->service->get($id)) abort(404);
        return view('admin.animals.images', compact('animal'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postImages($id, Request $request)
    {
        if (!$animal = $this->service->get($id)) abort(404);
        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $file) {
                try {
                    $path = public_path() . '/img/animals/' . $id . '/';
                    if (!file_exists($path)) {
                        mkdir($path, 666, true);
                    }

                    $name = rand(11111, 99999) . '.' . $file->getClientOriginalExtension();
                    $this->service->saveImage($animal, $name);
                    $image_resize = Image::make($file->getRealPath());
                    $image_resize->resize(800, 600);
                    $image_resize->save($path . $name);

                    $thumbnail = Image::make($file->getRealPath());
                    $thumbnail->resize(200, 150);
                    $thumbnail->save($path . 't_' . $name);
                } catch (FileNotFoundException $e) { }
            }
        }
        session()->flash('success', 'You have uploaded images.');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function deleteImage(Request $request)
    {
        $image = $request->image;
        $name  = explode('/', $image)[4];
        $this->service->deleteImage($name);

        if(File::exists($image)) {
            File::delete($image);
        }

        return json_encode('success');
    }

    /**
     * @param Request $request
     */
    public function sortImages(Request $request)
    {
        $this->service->sortImages($request->order);
    }
}
