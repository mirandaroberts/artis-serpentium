<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\BlogService;

class BlogController extends Controller
{
    /**
     * @var
     */
    protected $service;

    /**
     * AnimalsController constructor.
     * @param BlogService $service
     */
    public function __construct(BlogService $service)
    {
        $this->service = $service;
    }

    /**
     * Show the homepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->service->getAll();
        return view('admin.blog.index', compact('posts'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        if ($id == 'create') $post = $this->service->model;
        else $post = $this->service->get($id);
        return view('admin.blog.update', compact('post'));
    }

    /**
     * @param $id
     * @param BlogRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate($id, BlogRequest $request)
    {
        if ($id == 'create') $entity = $this->service->create($request->all());
        else $entity = $this->service->update($this->service->get($id), $request->all());
        if ($entity) session()->flash('success', 'Success');
        else session()->flash('error', 'There was an error with your request');
        return redirect()->back();
    }
}
