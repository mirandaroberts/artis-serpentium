<?php

namespace App\Http\Controllers;

use App\Services\AnimalService;

class AnimalController extends Controller
{
    /**
     * @var AnimalService
     */
    protected $service;

    /**
     * AnimalsController constructor.
     * @param AnimalService $service
     */
    public function __construct(AnimalService $service)
    {
        $this->service = $service;
    }

    /**
     * @param null $filter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getListings($filter = null)
    {
        $listings = $this->service->getForSale($filter);
        $categories = $this->service->getCategories();
        return view('animals.index', compact('listings', 'categories', 'filter'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAnimal($id)
    {
        if (!$animal = $this->service->get($id)) abort(404);
        if (!$animal->price || $animal->sold_for) abort(404);
        return view('animals.view', compact('animal'));
    }
}
