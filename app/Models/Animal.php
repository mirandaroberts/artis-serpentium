<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_number', 'title', 'description', 'categories', 'species', 'dob', 'price', 'gender', 'sold_to', 'sold_at',
        'sold_for', 'tracking_number', 'notes'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class)->orderBy('order','asc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function featuredImage()
    {
        return $this->hasOne(Image::class)->where('order', 1);
    }
}
