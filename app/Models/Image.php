<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * @var string
     */
    protected $table = 'animal_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'animal_id', 'image', 'order'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function animal()
    {
        return $this->belongsTo(Animal::class);
    }

    /**
     * @return string
     */
    public function getThumbnailAttribute()
    {
        return '<img class="thumb" src="' . $this->thumbUrl . '" alt="' . $this->animal->title . '">';
    }

    /**
     * @return string
     */
    public function getDisplayAttribute()
    {
        return '<img src="' . $this->url . '" alt="' . $this->animal->title . '">';
    }

    /**
     * @return string
     */
    public function getThumbUrlAttribute()
    {
        return '/img/animals/' . $this->animal->id . '/t_' . $this->image;
    }

    /**
     * @return string
     */
    public function getUrlAttribute()
    {
        return '/img/animals/' . $this->animal->id . '/' . $this->image;
    }
}
