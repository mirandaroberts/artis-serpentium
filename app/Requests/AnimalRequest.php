<?php

namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnimalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!auth()->user()) return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_number' => 'required',
            'title'       => 'required',
            'categories'  => 'required',
            'species'     => 'required',
            'gender'      => 'required',
            'price'       => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }
}