<?php

namespace App\Services;

use App\Models\Animal;
use App\Models\Image;
use Illuminate\Support\Facades\Cache;

class AnimalService extends Service
{
    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new Animal();
        $this->cache         = 'animals';
        $this->required      = ['item_number', 'title', 'description', 'categories', 'species', 'dob', 'price', 'gender',
            'sold_to', 'sold_at', 'sold_for', 'tracking_number', 'notes'];
    }

    /**
     * @param $animal
     * @param $image
     * @return bool
     */
    public function saveImage($animal, $image)
    {
        $image = Image::create([
            'animal_id' => $animal->id,
            'image'     => $image
        ]);

        $this->resetImgOrder($animal);
        $this->flushCache();

        if ($image) return true;
        return false;
    }

    /**
     * @param $name
     */
    public function deleteImage($name)
    {
        $image = Image::where('image', $name)->first();
        $animal = $this->get($image->animal_id);
        $this->flushCache();
        $image->delete();
        $this->resetImgOrder($animal);
    }

    /**
     * @param $animal
     */
    protected function resetImgOrder($animal)
    {
        foreach ($animal->images as $i => $image)
        {
            $image->order = $i + 1;
            $image->save();
        }
    }

    /**
     * @param $animal
     * @return int
     */
    protected function calcImageOrder($animal)
    {
        $imageCount = $animal->images->count();
        return $imageCount + 1;
    }

    /**
     * @param $orderArray
     */
    public function sortImages($orderArray)
    {
        foreach ($orderArray as $order => $id)
        {
            $order = $order + 1;
            $image = Image::where('id', $id)->first();
            if ($image) {
                $image->order = $order;
                $image->save();
            }
        }

        $this->flushCache();
    }

    /**
     * @param null $filter
     * @return mixed
     */
    public function getForSale($filter = null)
    {
        return Cache::tags($this->cache)->rememberForever('for_sale_animals', function () {
            return $this->model->where('price', '>', 0)
                               ->where('species')
                               ->paginate(10);
        });
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        $categories = array();
        $animals = $this->getAll();
        foreach ($animals as $animal) foreach (explode(', ', $animal->categories) as $category) {
            if (!in_array($category, $categories)) $categories[] = $category;
        }
        return $categories;
    }
}