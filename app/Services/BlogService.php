<?php

namespace App\Services;

use App\Models\BlogPost;

class BlogService extends Service
{
    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new BlogPost();
        $this->cache         = 'posts';
        $this->required      = ['title', 'slug', 'body', 'categories', 'tags'];
    }
}