<?php
namespace App\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

abstract class Service {

    /*
    |--------------------------------------------------------------------------
    | Base Service
    |--------------------------------------------------------------------------
    |
    | Base service will set up functions for other services.
    |
    */

    /**
     * Errors.
     * @var MessageBag
     */
    protected $errors = null;

    /**
     * Cache tag
     * @var string
     */
    protected $cache;

    /**
     * Relationships to include with queries
     * @var array
     */
    protected $relationships = [];

    /**
     * Required values to pass on creation
     * @var array
     */
    public $required = [];

    /**
     * Model class that service is working with
     * @var
     */
    public $model;

    /**
     * Home constructor.
     */
    public function __construct()
    {
        $this->callMethod('beforeConstruct');
        $this->resetErrors();
        $this->callMethod('afterConstruct');
    }

    /**
     * Calls a service method and injects the required dependencies.
     * @param $methodName
     * @param array $params
     * @return null
     */
    protected function callMethod($methodName, $params = array())
    {
        if (method_exists($this, $methodName)) return App::call([$this, $methodName], $params);
        else return null;
    }

    /*
    |--------------------------------------------------------------------------
    | Error Handling
    |--------------------------------------------------------------------------
    |
    | Sets up error handling for other services
    |
    */

    /**
     * Return if an error exists.
     * @return bool
     */
    public function hasErrors()
    {
        return $this->errors->count() > 0;
    }

    /**
     * Return if an error exists.
     * @return bool
     */
    public function hasError($key)
    {
        return $this->errors->has($key);
    }

    /**
     * Return errors.
     * @return MessageBag
     */
    public function errors()
    {
        return $this->errors;
    }
    /**
     * Return errors.
     * @return array
     */
    public function getAllErrors()
    {
        return $this->errors->unique();
    }

    /**
     * Returns error by key
     * @param $key
     * @return array
     */
    public function getError($key)
    {
        return $this->errors->get($key);
    }

    /**
     * Empty the errors MessageBag.
     * @return void
     */
    public function resetErrors()
    {
        $this->errors = new MessageBag();
    }

    /**
     * Add an error to the MessageBag.
     * @param string $key
     * @param string $value
     * @return void
     */
    protected function setError($key, $value)
    {
        $this->errors->add($key, $value);
    }

    /**
     * Add multiple errors to the message bag
     * @param MessageBag $errors
     * @return void
     */
    protected function setErrors($errors)
    {
        $this->errors->merge($errors);
    }

    /*
    |--------------------------------------------------------------------------
    | Database Functions
    |--------------------------------------------------------------------------
    |
    | Sets up generic methods for database handling and caching
    |
    */

    /**
     * Helper function: Implode array recursively
     * @param $array
     * @param $glue
     * @return bool|string
     */
    function multiImplode($array, $glue) {
        $ret = '';
        foreach ($array as $item) if (is_array($item)) $ret .= $this->multiImplode($item, $glue) . $glue;
        else $ret .= $item . $glue;
        $ret = substr($ret, 0, 0-strlen($glue));
        return $ret;
    }


    /**
     * Set model class
     * @param $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * Set cache tag
     * @param $cache
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
    }

    /**
     * Flushes entire cache tag
     */
    public function flushCache()
    {
        if ($this->cache) Cache::tags($this->cache)->flush();
    }

    /**
     * Forget cache instance by key
     * @param $key
     */
    public function flushCacheKey($key)
    {
        if ($this->cache) Cache::forget($key);
    }

    /**
     * Returns a slug of given string
     * @param string $value
     * @return string
     */
    protected function slug($value)
    {
        return str_replace(' ', '_', strtolower($value));
    }

    /**
     * Get all entries
     * @param string $orderBy
     * @param string $orderByMethod
     * @param bool $pagination
     * @return null
     */
    public function getAll($orderBy = 'id', $orderByMethod = 'desc', $pagination = false)
    {
        if ($this->model) {
            $cacheName = $this->cache . '_all_' . $orderBy . '_' . $orderByMethod;
            if ($pagination) $cacheName = $this->cache . '_all_' . $orderBy . '_' . $orderByMethod . '_' . $pagination;
            if ($this->cache) {
                return Cache::tags($this->cache)->rememberForever($cacheName, function () use ($orderBy, $orderByMethod, $pagination) {
                    if ($pagination) return $this->model->with($this->relationships)->orderBy($orderBy, $orderByMethod)->paginate($pagination);
                    else return $this->model->with($this->relationships)->orderBy($orderBy, $orderByMethod)->get();
                });
            } else {
                if ($pagination) return $this->model->with($this->relationships)->orderBy($orderBy, $orderByMethod)->paginate($pagination);
                else return $this->model->with($this->relationships)->orderBy($orderBy, $orderByMethod)->get();
            }
        } else return null;
    }

    /**
     * Get entries with where clause
     * @param $where
     * @param string $orderBy
     * @param string $orderByMethod
     * @param bool $pagination
     * @return null
     */
    public function getWhere($where, $orderBy = 'id', $orderByMethod = 'desc', $pagination = false)
    {
        if ($this->model) {
            $whereSlug = $this->multiImplode($where, '_');
            $cacheName = $this->cache . '_where_' . $whereSlug . '_' . $orderBy . '_' . $orderByMethod;
            if ($pagination) $cacheName = $this->cache . '_where_' . $whereSlug . '_' . $orderBy . '_' . $orderByMethod . '_' . $pagination;
            if ($this->cache) {
                return Cache::tags($this->cache)->rememberForever($cacheName, function () use ($orderBy, $orderByMethod, $pagination, $where) {
                    if ($pagination) return $this->model->where($where)->with($this->relationships)->orderBy($orderBy, $orderByMethod)->paginate($pagination);
                    else return $this->model->where($where)->with($this->relationships)->orderBy($orderBy, $orderByMethod)->get();
                });
            } else {
                if ($pagination) return $this->model->where($where)->with($this->relationships)->orderBy($orderBy, $orderByMethod)->paginate($pagination);
                else return $this->model->where($where)->with($this->relationships)->orderBy($orderBy, $orderByMethod)->get();
            }
        } else return null;
    }

    /**
     * Get one by row value, defaults to id
     * @param $value
     * @param string $row
     * @return null
     */
    public function get($value, $row = 'id')
    {
        if ($this->model) {
            $cacheName = $this->cache . '_' . $row . '_' . str_replace(' ', '_', strtolower($value));
            if ($this->cache) {
                return Cache::tags($this->cache)->rememberForever($cacheName, function () use ($row, $value) {
                    return $this->model->where($row, $value)->with($this->relationships)->first();
                });
            } else return $this->model->where($row, $value)->with($this->relationships)->first();
        } else return null;
    }

    /**
     * Get latest entry
     * @return null
     */
    public function getLatest()
    {
        if ($this->model) {
            if ($this->cache) {
                return Cache::tags($this->cache)->rememberForever($this->cache . '_latest', function () {
                    return $this->model->latest()->first();
                });
            } else return $this->model->latest()->first();
        } else return null;
    }

    /**
     * Deletes an instance of model
     * @param $entry
     * @param bool $force
     * @return bool
     */
    public function delete($entry, $force = false)
    {
        $this->callMethod('preDelete', $entry);
        if ($force) $entry->forceDelete();
        else $entry->delete();
        $this->flushCache();
        $this->callMethod('postDelete');
        return true;
    }

    /**
     * Create a new model entry. Expects validated/sanitized data
     * @param array $data
     * @return null
     */
    public function create(array $data)
    {
        $this->flushCache();
        if ($this->model) {
            $this->callMethod('preCreate', ['data' => $data]);
            $entry = $this->model->create($data);
            $this->callMethod('postCreate', ['entry' => $entry]);
            return $entry;
        } else return null;
    }

    /**
     * Updates an entry. Expects validated/sanitized data
     * @param $entry
     * @param array $data
     * @return mixed
     */
    public function update($entry, array $data)
    {
        $this->callMethod('preUpdate', [$entry, $data]);
        $entry->update($data);
        $this->callMethod('postUpdate', $entry);
        $this->flushCache();
        return $entry;
    }

    /**
     * Truncate the table
     */
    public function truncate()
    {
        DB::table($this->model->table)->truncate();
        $this->flushCache();
    }
}