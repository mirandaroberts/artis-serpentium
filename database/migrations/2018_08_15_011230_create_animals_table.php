<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_number')->unique();
            $table->string('title');
            $table->longText('description');
            $table->longText('categories');
            $table->string('species');
            $table->string('dob');
            $table->integer('price');
            $table->enum('gender', ['Male', 'Female', 'Unknown']);
            $table->string('sold_to');
            $table->timestamp('sold_at');
            $table->integer('sold_for');
            $table->string('tracking_number')->nullable();
            $table->longText('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
