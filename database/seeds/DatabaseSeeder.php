<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => 'Miranda Roberts',
            'email' => 'himowa@gmail.com',
            'password' => bcrypt('Goldenglow1'),
        ]);
    }
}
