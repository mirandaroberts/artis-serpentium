$(function() {

    var getType = (function() {

        var objToString = ({}).toString ,
            typeMap     = {},
            types = [ 
              "Boolean", 
              "Number", 
              "String",                
              "Function", 
              "Array", 
              "Date",
              "RegExp", 
              "Object", 
              "Error"
            ];

        for ( var i = 0; i < types.length ; i++ ){
            typeMap[ "[object " + types[i] + "]" ] = types[i].toLowerCase();
        };    

        return function( obj ){
            if ( obj == null ) {
                return String( obj );
            }
            // Support: Safari <= 5.1 (functionish RegExp)
            return typeof obj === "object" || typeof obj === "function" ?
                typeMap[ objToString.call(obj) ] || "object" :
                typeof obj;
        }
    }());

    $('[data-toggle]').each(function() {
        var $this = $(this);
        var init = function(_options) {
            _options.e.stopPropagation();
            var $target = $(_options.selector);
            $target.toggleClass(_options.class);
        }
        $this.click(function(e) {
            var options = $this.data('toggle');
            if (getType(options) === "array") {
                for (i = 0; i < options.length; i++) {
                    options[i].e = e;
                    init(options[i]);
                }
            }
            else {
                options.e = e;
                init(options);
            }
        });
    });

    $('[data-remove]').each(function() {
        var $this = $(this);
        var init = function(_options) {
            _options.e.stopPropagation();
            var $target = $(_options.selector);
            $target.removeClass(_options.class);
        }
        $this.click(function(e) {
            var options = $this.data('remove');
            if (getType(options) === "array") {
                for (i = 0; i < options.length; i++) {
                    options[i].e = e;
                    init(options[i]);
                }
            }
            else {
                options.e = e;
                init(options);
            }
        });
    });

    $('[data-add]').each(function() {
        var $this = $(this);
        var init = function(_options) {
            _options.e.stopPropagation();
            var $target = $(_options.selector);
            $target.addClass(_options.class);
        }
        $this.click(function(e) {
            var options = $this.data('add');
            if (getType(options) === "array") {
                for (i = 0; i < options.length; i++) {
                    options[i].e = e;
                    init(options[i]);
                }
            }
            else {
                options.e = e;
                init(options);
            }
        });
    });

    $('.carousel').each(function() {
        var $this = $(this);
        var $prev = $this.find('.prev');
        var $next = $this.find('.next');
        var $items = $this.find('.item');
        var totalItems = $items.length; // 4
        var totalIndex = totalItems - 1; // 3

        // Index
        var activeIndex = 0; // 3 (4th item)
        var $activeItem = $items.filter('.active');
        if ($activeItem.length > 0) {
            activeItem = $activeItem.index();
        }

        var prevIndex = function() {
            // If active item, is first item, go to last item
            if (activeIndex <= 0) {
                activeIndex = totalIndex;
            }
            else {
                activeIndex--;
            }
        }

        var nextIndex = function() {
            // If active item, is last item, go back to first item
            if (activeIndex >= totalIndex) {
                activeIndex = 0;
            }
            else {
                activeIndex++;
            }
        }

        var render = function() {
            $activeItem = $items.eq(activeIndex);
            $items.removeClass('active');
            $activeItem.addClass('active');
        }

        $prev.click(function() {
            prevIndex();
            render();
        });

        $next.click(function() {
            nextIndex();
            render();
        });
    });

    /*************************
     * Handle SVG
     *************************/
    $('img[src$=".svg"]').each(function() {
        var $img = jQuery(this);
        var imgURL = $img.attr('src');
        var attributes = $img.prop("attributes");

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Remove any invalid XML tags
            $svg = $svg.removeAttr('xmlns:a');

            // Loop through IMG attributes and apply on SVG
            $.each(attributes, function() {
                $svg.attr(this.name, this.value);
            });

            // Replace IMG with SVG
            $img.replaceWith($svg);
        }, 'xml');
    });

    /*************************
     * Cookie Consent
     *************************/
    if (getCookie('cookie-consent') !== 'true') {
        $('#cookie-consent').slideDown();
    }

    $('.cookie-consent-btn').click(function() {
        $('#cookie-consent').slideUp();
        setCookie('cookie-consent', 'true', 365);
    });

});


/*************************
 * Set a cookie
 * @param cname
 * @param cvalue
 * @param exdays
 *************************/
function setCookie(cname, cvalue, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/*************************
 * Get a cookie
 * @param cname
 * @returns {string}
 *************************/
function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}