//
// /**
//  * First we will load all of this project's JavaScript dependencies which
//  * includes Vue and other libraries. It is a great starting point when
//  * building robust, powerful web applications using Vue and Laravel.
//  */
//
// require('./bootstrap');
//
// window.Vue = require('vue');
//
// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */
//
// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

$('document').ready(function() {



    /*************************
     * Nav Menu
     *************************/
    $('html').click(function() {
        let subnavs = $('.subnav');
        subnavs.hide();
    });

    $('.menu ul li a').click(function(e) {
        let subnav = $(this).next('ul');
        let others = $(this).closest('.menu').find('ul').not(subnav);

        others.hide();
        subnav.toggle();

        e.stopPropagation();
    });

    /*************************
     * Cookie Consent
     *************************/
    if (getCookie('cookie-consent') !== 'true') {
        $('#cookie-consent').slideDown();
    }

    $('.cookie-consent-btn').click(function() {
        $('#cookie-consent').slideUp();
        setCookie('cookie-consent', 'true', 365);
    });

    /*************************
     * Featured Cards
     *************************/
    $('.featured-card').mouseenter(function() {
        let tooltip = $(this).prev('.featured-tooltip');
        $(tooltip).slideDown();
    });

    $('.featured-card').mouseleave(function() {
        let tooltip = $(this).prev('.featured-tooltip');
        $(tooltip).slideUp();
    });

    /*************************
     * Handle SVG
     *************************/
    $('img[src$=".svg"]').each(function() {
        var $img = jQuery(this);
        var imgURL = $img.attr('src');
        var attributes = $img.prop("attributes");

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Remove any invalid XML tags
            $svg = $svg.removeAttr('xmlns:a');

            // Loop through IMG attributes and apply on SVG
            $.each(attributes, function() {
                $svg.attr(this.name, this.value);
            });

            // Replace IMG with SVG
            $img.replaceWith($svg);
        }, 'xml');
    });

    /*************************
     * Handle Datatables
     *************************/
    $('datatables').DataTable();
});

/*************************
 * Set a cookie
 * @param cname
 * @param cvalue
 * @param exdays
 *************************/
function setCookie(cname, cvalue, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/*************************
 * Get a cookie
 * @param cname
 * @returns {string}
 *************************/
function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
