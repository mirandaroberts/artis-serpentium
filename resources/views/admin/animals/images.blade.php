@extends('layouts.interior')

@section('header')
    <style>
        section {
            position: relative;
            width: 800px;
            min-height: 500px;
            margin: 20px auto;
            border: 1px solid #444;
            border-radius: 4px;
            box-shadow: inset 0 0 6px rgba(0,0,0,0.2);
            background-color: #555;
        }

        form {
            position: absolute;
            z-index: 10;
            top: 10px;
            right: 10px;
        }

        #gallery {
            margin: 10px;
            padding: 0;
            overflow: hidden;
        }

        #gallery li {
            list-style: none;
            float: left;
        }

        #gallery li img {
            margin: 10px;
            background-color: #eee;
            box-shadow: 0 0 6px rgba(0,0,0,0.2);
        }

        #image-box {
            /* top: 50%;
             font-size: 35px;
             left: 50%;*/
            position: absolute;
            top: 45px;
            left: 90px;
            /*transform: translate(-50%, -50%);*/
        }

        #image-box img {
            border: 8px solid #777;
            background-color: #eee;
            box-shadow: 0 0 6px rgba(0,0,0,0.2);
        }

        .column-3 {
            width: 33.33%;
            text-align: center;
        }

        .thumb {
            cursor: pointer;
            width: 240px;
        }

        .largeImage {
            width: 600px;
            height: 400px;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <h1>Animals - {{ $animal->title }} Images</h1>
    </div>

    <p>
        This page will be a little gallery thing, I already have some basic js and styles. The idea being you click on a
        thumbnail and it will slow it in the side gallery and drag and drop to reorder. Let's work on this together.
    </p>

    <form action="" method="post" enctype="multipart/form-data">
        @csrf

        <label for="files">Upload Images Here</label>
        <input type="file" id="files" name="files[]" multiple>
        <button type="submit" class="btn">Upload</button>
    </form>

    <section>
        <div id="image-box">
            {!! $animal->featuredImage->display !!}
            <button class="delete">x</button>
        </div>
    </section>

    <section>
        <ul id="gallery">
            @foreach ($animal->images as $image)
                <li class="column-3" id="{{ $image->id }}">{!! $image->thumbnail !!}</li>
            @endforeach
        </ul>
    </section>

    @include('layouts.partials._admin_sidebar')
@endsection

@section('scripts')
    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {
            $(".thumb").on('click', function(){
                var largeImage = $(this).attr("src");
                $("#image-box img").attr({src: largeImage});
            });

            $(".delete").on('click', function() {
                if (confirm("Are you sure you want to delete this image?") == true) {
                    deleteImage();
                }
            });

            function deleteImage() {
                var data = {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'image': $("#image-box img").attr("src")
                };

                $.ajax({
                    url: '/ajax/delete-image',
                    method: 'post',
                    data: data,
                    success: function(data) {
                        // remove image from the gallery
                    }
                });
            }

            $(function() {
                $("#gallery").sortable({
                    update: function (event, ui) {
                        var data = {
                            '_token': $('meta[name="csrf-token"]').attr('content'),
                            'order': $(this).sortable('toArray')
                        };

                        console.log(data);

                        $.ajax({
                            data: data,
                            type: 'post',
                            url: '/ajax/sort-images'
                        });
                    }
                });
                $("#gallery").disableSelection();
            });

            $(function() {
                $( "#image" ).resizable();
            });
        });
    </script>
@endsection
