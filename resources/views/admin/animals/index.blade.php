@extends('layouts.interior')

@section('content')
    <div class="container">
        <h1>Animals</h1>
    </div>

    <div>
        <a href="{{ route('admin.animals.update', ['id' => 'create']) }}" class="btn">Add Animal</a>
    </div>

    <table class="datatables">
        <thead>
        <tr>
            <th>Image</th>
            <th>Item #</th>
            <th>Title</th>
            <th>Price</th>
            <th>Status</th>
            <th>Options</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($animals as $animal)
                <tr>
                    <td></td>
                    <td>{{ $animal->item_number }}</td>
                    <td>{{ $animal->title }}</td>
                    <td>{{ $animal->price }}</td>
                    <td>{{ $animal->status }}</td>
                    <td>
                        <a href="{{ route('admin.animals.update', ['id' => $animal->id]) }}" class="button">Update</a>
                        <a href="{{ route('admin.animals.images', ['id' => $animal->id]) }}" class="button">Images</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    @include('layouts.partials._admin_sidebar')
@endsection
