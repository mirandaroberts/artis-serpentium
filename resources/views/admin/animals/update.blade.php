@extends('layouts.interior')

@section('content')
    <div class="container">
        <h1>Animals - @if (isset($animal->id)) Update {{ $animal->title }} @else Create @endif</h1>
    </div>

    <form action="" method="post">
        @csrf

        <div class="flex vertical padded">
            <div>
                <label for="item_number" class="label">Item Number *</label>
                <input type="text" id="item_number" name="item_number" value="{{ old('item_number', $animal->item_number) }}" required>
            </div>

            <div>
                <label for="title" class="label">Title *</label>
                <input type="text" id="title" name="title" value="{{ old('title', $animal->title) }}" required>
            </div>

            <div>
                <label for="description" class="label">Description</label>
                <textarea id="description" name="description">{{ old('description', $animal->description) }}</textarea>
            </div>

            <div>
                <label for="categories" class="label">Categories *</label>
                <input type="text" id="categories" name="categories" value="{{ old('categories', $animal->categories) }}" required>
                <small>Separate with commas</small>
            </div>

            <div>
                <label for="species" class="label">Species *</label>
                <input type="text" id="species" name="species" value="{{ old('species', $animal->species) }}" required>
            </div>

            <div>
                <label for="gender" class="label">Gender *</label>
                <select name="gender" id="gender" required>
                    <option value="Unknown" @if (old('gender', $animal->gender) == 'Unknown') selected @endif>Unknown</option>
                    <option value="Male" @if (old('gender', $animal->gender) == 'Male') selected @endif>Male</option>
                    <option value="Female" @if (old('gender', $animal->gender) == 'Female') selected @endif>Female</option>
                </select>
            </div>

            <div>
                <label for="dob" class="label">Date of Birth</label>
                <input type="text" id="dob" name="dob" value="{{ old('dob', $animal->dob) }}">
            </div>

            <div>
                <label for="price" class="label">Price *</label>
                <input type="number" min="0" step="any" id="price" name="price" value="{{ old('price', $animal->price) }}" required>
            </div>

            <div>
                <label for="notes" class="label">Notes</label>
                <textarea id="notes" name="notes">{{ old('notes', $animal->notes) }}</textarea>
            </div>

            @if (isset($animal->id))
                <hr>
                <h3>Sale Info</h3>

                <div>
                    <label for="sold_to" class="label">Sold To</label>
                    <input type="text" id="sold_to" name="sold_to" value="{{ old('sold_to', $animal->sold_to) }}">
                </div>

                <div>
                    <label for="sold_at" class="label">Sold At</label>
                    <input type="date" id="sold_at" name="sold_at" value="{{ old('sold_at', $animal->sold_at) }}">
                </div>

                <div>
                    <label for="sold_for" class="label">Sold For</label>
                    <input type="number" min="0" step="any" id="sold_for" name="sold_for" value="{{ old('sold_for', $animal->sold_for) }}">
                </div>

                <div>
                    <label for="tracking_number" class="label">Tracking Number</label>
                    <input type="text" id="tracking_number" name="tracking_number" value="{{ old('tracking_number', $animal->tracking_number) }}">
                </div>

                <hr>
            @endif

            <div>
                <button type="submit" class="btn">Submit</button>
            </div>
        </div>
    </form>

    @include('layouts.partials._admin_sidebar')
@endsection
