@extends('layouts.interior')

@section('content')
    <div class="container">
        <h1>Blog Posts</h1>
    </div>

    <div>
        <a href="{{ route('admin.blog.update', ['id' => 'create']) }}" class="btn">Create Post</a>
    </div>

    <table class="datatables">
        <thead>
        <tr>
            <th>Title</th>
            <th>Slug</th>
            <th>Date</th>
            <th>Options</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($posts as $post)
            <tr>
                <td>{{ $post->title }}</td>
                <td>{{ $post->slug }}</td>
                <td>{{ $post->create_at->format('m-d-Y') }}</td>
                <td>
                    <a href="{{ route('admin.blog.update', ['id' => $blog->id]) }}" class="button">Update</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @include('layouts.partials._admin_sidebar')
@endsection
