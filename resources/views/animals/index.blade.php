@extends('layouts.interior')

@section('content')
    <div class="padding padding-top-large padding-bottom-large">
        <h1>
            Listings
            @if ($filter)
                 - {{ $filter }}
            @endif
        </h1>

        <div class="flex padded">
            <div id="sidebar">
                <div class="border-right border round">
                    <div class="flex vertical">
                        <div class="padding border-bottom">
                            <input type="text" class="txt" name="listing-search" placeholder="Search Listings..."/>
                        </div>
                        <div class="padding">
                            <nav class="menu">
                                <ul>
                                    <li><a href><span class="text">Example Cat 1</span></a></li>
                                    <li><a href><span class="text">Example Cat 2</span></a></li>
                                    <li><a href><span class="text">Example Cat 3</span></a></li>
                                    <li><a href><span class="text">Example Cat 4</span></a></li>
                                    <li><a href><span class="text">Example Cat 5</span></a></li>
                                    @foreach ($categories as $category)
                                        <li>
                                            <a href="/animals/{{ $category }}">
                                                <span class="text">{{ ucwords($category) }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="flex vertical padded">

                    <div>
                        <div class="listing padding-bottom margin-bottom border-bottom">
                            <div class="flex padded align-center">
                                <div>
                                    <img src="https://placehold.it/128x128" />
                                </div>
                                <div class="box align-left">
                                    <h3>Example animal</h3>
                                    <h5>1/1/2018 | Male | Ball Python</h5>
                                    <span class="price">$25</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if ($listings)
                        @foreach ($listings as $listing)
                            <div class="listing">
                                <a href="/animals/view/{{ $listing->id }}">
                                    {!! $listing->featuredImage->thumbnail !!}
                                    <h3>{{ $listing->title }}</h3>
                                    <h5>{{ $listing->dob }} | {{ $listing->gender }} | {{ $listing->species }}</h5>
                                    <span>{{ $listing->price }}</span>
                                </a>
                            </div>
                        @endforeach

                        {{ $listings->links() }}
                    @else
                        <p>There is currently nothing available. <a href="/season">Check out what's coming next.</a></p>
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection
