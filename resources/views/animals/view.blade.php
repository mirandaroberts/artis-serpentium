@extends('layouts.interior')

@section('content')
    <div class="container">
        <h1>
            Listings - {{ $animal->title }}
        </h1>

        <section>
            <div id="image-box">
                {!! $animal->featuredImage->display !!}
            </div>
        </section>

        <section>
            <ul id="gallery">
                @foreach ($animal->images as $image)
                    <li class="column-3" id="{{ $image->id }}">{!! $image->thumbnail !!}</li>
                @endforeach
            </ul>
        </section>

        {{ $animal->item_number }}
        {{ $animal->species }}
        {{ $animal->price }}
        {{ $animal->gender }}
        {{ $animal->dob }}
        {{ $animal->description }}

        <a href="">Inquire about this animal</a>
    </div>
@endsection
