@extends('layouts.interior')

@section('content')
<div class="padding-large">

    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        <div class="flex padded vertical">
            <div>
                <label for="email" class="label">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="txt{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <div class="alert error" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif
            </div>
            <div>
                <label for="password" class="label">{{ __('Password') }}</label>
                <input id="password" type="password" class="txt{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                    <div class="alert error" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </div>
                @endif
            </div>
            <div>
                <div class="checkbox">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
            <div>
                <div class="flex padded align-center">
                    <div>
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>
                    </div>
                    <div>

                        <a href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
