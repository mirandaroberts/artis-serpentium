@extends('layouts.app')

<div id="interior">
    <header id="header">
        <div class="max">
        	<div class="flex padded nowrap align-center">
        		<div>
        			@include('layouts.partials._logo')
        		</div>
        		<div class="box flex justify-end">
            		@include('layouts.partials._nav')
            	</div>
            </div>
        </div>
    </header>

    <div id="app">
        <div class="max">
            @foreach (['error', 'warning', 'success', 'info'] as $key)
                @if(session()->has($key))
                    <p class="alert alert-{{ $key }}">{{ session()->get($key) }}</p>
                @endif
            @endforeach

            @yield('content')
        </div>
    </div>
</div>

@include('layouts.partials._footer')