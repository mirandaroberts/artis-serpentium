<div role="dialog" id="cookie-consent" class="flex padded justify-center items-center shadow" style="display: none;">
    <div class="text-white">
        We use Cookies for user analysis and on-page improvements!

        <a role="button" href="http://cookiesandyou.com" target="_blank" rel="noopener">
            Learn about cookies
        </a>
    </div>
    <div class="col-btn">

        <button type="button" class="btn transparent cookie-consent-btn">
            Got it!
        </button>
    </div>
</div>