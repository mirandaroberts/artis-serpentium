<footer id="footer">
    <div class="flex padded-large justify-center align-center medium-vertical">

        <div>
            <div class="flex vertical padded">
                <div class="medium-align-left">
                    @include('layouts.partials._logo')
                </div>
            </div>
        </div>

        <div>
            <form action="/newsletter" method="post">
                @csrf
                <div class="flex vertical padded align-center">
                    <div>
                        <label for="email">Sign up to be notified of new projects and clutches</label>
                    </div>
                    <div>
                        <div class="flex padded align-center justify-center">
                            <div>
                                <input type="email" class="txt" id="email" style="width: 284px;" placeholder="Email address..." name="email">
                            </div>
                            <div>
                                <input type="submit" class="btn blue btn-sign-up" value="Sign Up">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div>
            <div class="flex justify-center medium-justify-start">
                <nav class="menu horizontal">
                    <ul>
                        <li><a href="https://www.facebook.com/artisserpentium/"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/artisserpentium"><i class="fab fa-twitter-square"></i></a></li>
                        <li><a href="https://www.instagram.com/artisserpentium/"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="https://artis-serpentium.tumblr.com/"><i class="fab fa-tumblr-square"></i></a></li>
                    </ul>
                </nav>
            </div>
            <div class="align-center margin-top-small">
                <a class="btn outline btn-contact-us" href="mailto:contact@artis-serpentium.com">Contact Us</a>
            </div>
        </div>

        <div class="width-12 align-center">
            <p>Copyright 2018 Artis Serpentium &copy; All rights reserved. <a href>Privacy Policy</a> <i class="fas fa-circle"></i> <a href>Terms of Service</a> <i class="fas fa-circle"></i> <a href>USARK</a> <i class="fas fa-circle"></i> <a href>Reptally</a></p>
        </div>

    </div>
</footer>