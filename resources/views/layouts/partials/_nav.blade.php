<nav class="menu">
    <a class="toggle-menu" data-add='[{"selector": "#menu", "class": "open"},{"selector": "body", "class": "menuOpen"}]'>
        <i class="fas fa-bars"></i>
    </a>
    <ul id="menu" data-click-body>
        <li>
            <a data-toggle='{"selector": "#menuAvailable", "class": "open"}'>
                <span class="text">Available</span>
                <span class="icon"><i class="fas fa-caret-right"></i></span>
            </a>
            <ul id="menuAvailable" data-click-body>
                <li class="back"><a data-toggle='{"selector": "#menuAvailable", "class": "open"}'><span class="icon"><i class="fas fa-caret-left"></i></span><span class="text">Back</span></a></li>
                <li><a href="/animals"><span class="text">All Animals</span></a></li>
                <li><a href="/animals/ball-pythons"><span class="text">Ball Pythons</span></a></li>
                <li><a href="/animals/leopard-geckos"><span class="text">Leopard Geckos</span></a></li>
                <li><a href="/animals/other"><span class="text">Other Reptiles</span></a></li>
                <li><a href="/merch"><span class="text">Merchandise</span></a></li>
            </ul>
        </li>
        <li>
            <a href="/season"><span class="text">{{ \Carbon\Carbon::now()->format('Y') }} Season</span></a>
        </li>
        <li>
            <a href="/caresheets"><span class="text">Caresheets</span></a>
        </li>
        <li>
            <a data-toggle='{"selector": "#menuAbout", "class": "open"}'>
                <span class="text">About</span>
                <span class="icon"><i class="fas fa-caret-right"></i></span>
            </a>
            <ul id="menuAbout" data-click-body>
                <li class="back"><a data-toggle='{"selector": "#menuAbout", "class": "open"}'><span class="icon"><i class="fas fa-caret-left"></i></span><span class="text">Back</span></a></li>
                <li><a href="/about"><span class="text">About Us</span></a></li>
                <li><a href="/shipping"><span class="text">Shipping Policy</span></a></li>
                <li><a href="/payment-plans"><span class="text">Payment Plans</span></a></li>
                <li><a href="/testimonials"><span class="text">Testimonials</span></a></li>
            </ul>
        </li>
        <li>
            <a href="/blog"><span class="text">Blog</span></a>
        </li>
        @if (auth()->check())
            <li>
                <a data-toggle='{"selector": "#menuAdmin", "class": "open"}'>
                    <span class="text">Admin</span>
                    <span class="icon"><i class="fas fa-caret-right"></i></span>
                </a>
                <ul id="menuAdmin" data-click-body>
                    <li class="back"><a data-toggle='{"selector": "#menuAdmin", "class": "open"}'><span class="icon"><i class="fas fa-caret-left"></i></span><span class="text">Back</span></a></li>
                    <li><a href="/admin"><span class="text">Dashboard</span></a></li>
                    <li><a href="/admin/animals"><span class="text">Manage Animals</span></a></li>
                    <li><a href="/admin/merch"><span class="text">Manage Merchandise</span></a></li>
                    <li><a href="/admin/blog"><span class="text">Manage Blog</span></a></li>
                </ul>
            </li>
        @endif
    </ul>
</nav>