@extends('layouts.app')
@section('bodyClass', 'page-home')

<header id="header">
	<div class="max">
		<div class="wrapper">
			@include('layouts.partials._logo')
			<div class="nav-wrapper">
				@include('layouts.partials._nav')
			</div>
		</div>
	</div>
	<video autoplay muted loop id="video">
		<source src="/videos/slider.mp4" type="video/mp4">
	</video>
</header>

<main>
	<section class="section padding padding-top-large padding-bottom-large" id="section-featured">
		<div class="max">
			<h2 class="align-center">Featured Reptiles</h2>
			<div class="carousel">
				<div class="prev"><i class="fas fa-angle-left"></i></div>
				<div class="items">
					<div class="item active">
						<div class="thumbnail">
							<img src="http://placehold.it/400x400" />
						</div>
						<h3 class="margin-bottom-xsmall">
							<span class="text">Reptile Name 1</span>
							<span class="icon"><i class="fas fa-venus"></i></span>
						</h3>
						<div class="price">$400.00</div>
					</div>
					<div class="item">
						<div class="thumbnail">
							<img src="http://placehold.it/400x400" />
						</div>
						<h3 class="margin-bottom-xsmall">
							<span class="text">Reptile Name 2</span>
							<span class="icon"><i class="fas fa-venus"></i></span>
						</h3>
						<div class="price">$400.00</div>
					</div>
					<div class="item">
						<div class="thumbnail">
							<img src="http://placehold.it/400x400" />
						</div>
						<h3 class="margin-bottom-xsmall">
							<span class="text">Reptile Name 3</span>
							<span class="icon"><i class="fas fa-venus"></i></span>
						</h3>
						<div class="price">$400.00</div>
					</div>
					<div class="item">
						<div class="thumbnail">
							<img src="http://placehold.it/400x400" />
						</div>
						<h3 class="margin-bottom-xsmall">
							<span class="text">Reptile Name 4</span>
							<span class="icon"><i class="fas fa-venus"></i></span>
						</h3>
						<div class="price">$400.00</div>
					</div>
				</div>
				<div class="next"><i class="fas fa-angle-right"></i></div>
			</div>
			<div class="padding-large align-center">
				<a class="btn outline">
					<span class="text">View All</span>
				</a>
			</div>
		</div>
	</section>
	<section class="section" id="section-about">
		<div class="max">
			<div class="flex align-center">
				<div class="width-6 large-none">
					<img src="http://placehold.it/800x800" />
				</div>
				<div class="width-6 large-12 padding-large align-left">
					<h2>About Artis Serpentium</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<div class="algin-center">
						<a class="btn brown">
							<span class="text">View All Reptiles</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section padding padding-top-large padding-bottom-large" id="section-blog">
		<div class="max">
			<h2 class="align-center">Herpetology Report</h2>
			<div class="flex padded">
				<div class="width-4 medium-6 small-12">
					<div class="card">
						<div class="thumbnail">
							<img src="http://placehold.it/400x200" class="width-12" />
						</div>
						<div class="padding">
							<h3>Lorem Ipsum</h3>
							<div class="meta">
								<span class="date">Date</span>
								<span class="seperator"><i class="fas fa-circle"></i></span>
								<span class="category">Category</span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat.</p>
							<a class="btn outline">
								<span class="text">Read More</span>
								<span class="icon"><i class="fas fa-plus"></i></span>
							</a>
						</div>
					</div>
				</div>
				<div class="width-4 medium-6 small-12">
					<div class="card">
						<div class="thumbnail">
							<img src="http://placehold.it/400x200" class="width-12" />
						</div>
						<div class="padding">
							<h3>Lorem Ipsum</h3>
							<div class="meta">
								<span class="date">Date</span>
								<span class="seperator"><i class="fas fa-circle"></i></span>
								<span class="category">Category</span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat.</p>
							<a class="btn outline">
								<span class="text">Read More</span>
								<span class="icon"><i class="fas fa-plus"></i></span>
							</a>
						</div>
					</div>
				</div>
				<div class="width-4 medium-6 small-12">
					<div class="card">
						<div class="thumbnail">
							<img src="http://placehold.it/400x200" class="width-12" />
						</div>
						<div class="padding">
							<h3>Lorem Ipsum</h3>
							<div class="meta">
								<span class="date">Date</span>
								<span class="seperator"><i class="fas fa-circle"></i></span>
								<span class="category">Category</span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat.</p>
							<a class="btn outline">
								<span class="text">Read More</span>
								<span class="icon"><i class="fas fa-plus"></i></span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="align-center padding-large">
				<a class="btn outline">
					<span class="text">View All Reptiles</span>
				</a>
			</div>
		</div>
	</section>
</main>

@include('layouts.partials._footer')