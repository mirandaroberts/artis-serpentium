<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

// Ajax Routes...
Route::prefix('ajax')->group(function () {
    Route::post('/delete-image', 'Admin\AnimalsController@deleteImage')->middleware('auth');
    Route::post('/sort-images', 'Admin\AnimalsController@sortImages')->middleware('auth');
});

// Animal Routes...
Route::prefix('animals')->group(function () {
    Route::get('/view/{id}', 'AnimalController@getAnimal');
    Route::get('/{filter?}', 'AnimalController@getListings');
});

// Authentication Routes...
Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => '', 'uses' => 'Auth\LoginController@login']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

// Password Reset Routes...
Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('password/reset', ['as' => 'password.request', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('password/reset', ['as' => '', 'uses' => 'Auth\ResetPasswordController@reset']);
Route::get('password/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);

// Admin Routes
Route::prefix('admin')->middleware('auth')->namespace('Admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('admin.index');

    // Animals
    Route::prefix('animals')->group(function () {
        Route::get('/', 'AnimalsController@index')->name('admin.animals.index');
        Route::get('/{id}', 'AnimalsController@getUpdate')->name('admin.animals.update');
        Route::post('/{id}', 'AnimalsController@postUpdate');
        Route::get('/images/{id}', 'AnimalsController@getImages')->name('admin.animals.images');
        Route::post('/images/{id}', 'AnimalsController@postImages');
    });

    // Caresheets
    Route::prefix('caresheets')->group(function () {
        Route::get('/', 'CaresheetController@index')->name('admin.caresheets.index');
    });

    // Blog
    Route::prefix('blog')->group(function () {
        Route::get('/', 'BlogController@index')->name('admin.blog.index');
        Route::get('/{id}', 'BlogController@getUpdate')->name('admin.blog.update');
        Route::post('/{id}', 'BlogController@postUpdate');
    });

    // Season
    Route::prefix('season')->group(function () {
        Route::get('/', 'SeasonController@index')->name('admin.season.index');
    });

    // Merchandise
    Route::prefix('merch')->group(function () {
        Route::get('/', 'MerchController@index')->name('admin.merch.index');
    });
});

// Blog Routes
Route::prefix('blog')->group(function () {
    Route::get('/', 'BlogController@index')->name('blog.index');
});

// Caresheet Routes
Route::prefix('caresheets')->group(function () {
    Route::get('/', 'CaresheetController@index')->name('caresheets.index');
});

// Single Pages
Route::get('season', 'HomeController@season')->name('season');
